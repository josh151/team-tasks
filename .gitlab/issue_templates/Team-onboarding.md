<!--
# READ ME FIRST

This issue is a resource in addition to the general on-boarding task.
It is meant to create an easier overview of what you need to do to get
up to speed to team related tasks faster.

You are still required to finish the general on-boarding task even if
you close this issue. Once both of the issues are closed, you can
consider your onboarding finished.

-->

# New team member

Welcome to the Distribution team!

To get you up to speed quicker so you can start contributing to the team efforts, this on-boarding issue will try to provide a few tips on how to navigate around the team resources as well as company resources.

First, you are likely to want to know where your place is in the company structure. For this you would want to check out [team chart](https://about.gitlab.com/team/chart/).

## Company

Most of the information you will ever need is listed in your general onboarding
document. However, there is a lot of things to digest in there so let's highlight few items that you should focus on.

[Handbook](https://about.gitlab.com/handbook/) is a great resource for all things GitLab.
Understanding the [Values](https://about.gitlab.com/handbook/values/) is really important as that sets the tone for all interactions that you will have in the company. If anything is unclear about it, feel free to ask a question in your next 1-1 with your Engineering Manager.
Sooner or later you will have some money related question, so be sure to checkout
[spending company money page](https://about.gitlab.com/handbook/spending-company-money/). Finally, [general guidelines](https://about.gitlab.com/handbook/general-guidelines/) will help you understand more of why you are seeing some of the behaviours you might not have
encountered in your prior companies.

Of course, it would be great to go through the whole handbook but that is going to be really difficult given the size of it so don't feel bad if you can't read through
all of it. Remember that you can always search!
For now though, you should get to know your team a bit better.

## Team

Each team has it's own handbook section, and Distribution is no exception.
Read through the whole [Distribution handbook](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/)
section please!
Consider this your first team task, understanding the Mission, Vision and how work is executed within the team. If you find any typo's or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/engineering/development/enablement/distribution/index.html.md) by submitting a Merge Request. This is how you can start a discussion with your colleagues. If you would rather start a discussion first, you can go to the
[Distribution team tasks issue tracker](https://gitlab.com/gitlab-org/distribution/team-tasks) and submit an issue there.


## Team Onboarding Tasks

- [ ] New team member: Review [Distribution handbook section](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/).
- [ ] New team member: Check out [team projects](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/#projects).
- [ ] New team member: After joining Slack, join the following channels:
    - #g_distribution: This is where team discussions and other groups make contact with the Distribution team.
    - #eng-week-in-review: This is mandatory for the engineering organization.
    - #kubernetes: This is a useful channel for any Distribution team member.
    - #loc_*: There are location based channels for employees all over the world. Look through the channels that start with `loc_` to find the one for your area.
- [ ] New team member: Verify that you have access to [all work resources](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/#work-resources) listed.
- [ ] New team member: Read the [Keeping yourself informed](https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed) documentation.
- [ ] New team member: Complete the [Secure Coding training](https://about.gitlab.com/handbook/engineering/security/secure-coding-training.html).
- [ ] Manager: Submit access request for the following GCP projects:
  - testground
  - cloud-native
  - omnibus-runners
- [ ] Manager: Request Red Hat account for new team member
- [ ] New team member: Create an account at [ArtifactHub.io](https://artifacthub.io) using your GitLab email account. Once the account has been confirmed (link sent to email), inform your manager so that the account can be associated with the GitLab organization. 
- [ ] New team member: Review and familiarize yourself with the Engineering [workflow](https://about.gitlab.com/handbook/engineering/workflow/)
- [ ] Manager: Add new team member to the Distribution time off calendar.
- [ ] Onboarding Buddy: Create AWS account for new team member.
- [ ] New team member: Submit access request for yourself to have access to the following
    - [ ] 1Password vaults
      - `build`
      - `cloud native`
      - `cloud images`
    - [ ] Repositories
      - [ ] Developer access to [chef-repo](https://ops.gitlab.net/gitlab-cookbooks/chef-repo)
    - [ ] Customer support tickets in [Zendesk](https://about.gitlab.com/handbook/support/internal-support/#viewing-support-tickets)

For access requests, you can submit the access requests by creating a [new issue](https://gitlab.com/gitlab-com/access-requests/issues/new) in gitlab-com/access-requests. Use the New Access Request template for the issue, but this template can be a bit daunting and you can create a comment on this issue to receive some assistance.

## Staying informed

There is a lot of information flowing around, but to stay on top of most important things you should be a part of `#company-announcements` channel in Slack. You are likely to have received an invite for the `Company call`, but don't feel bad if you can't attend it. Just checking the [Company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) should be enough to keep you on top of most important events.

As for the team events, you are not obligated to be part of any of the meetings
that you are invited to. However, please note your absence by responding to invites in time. Not attending the meeting is no excuse for not reading the
agenda items or going through the notes of the meeting.

## On-boarding experience summary

To share some new found knowledge, the rest of the Distribution team would love
to hear your experience.

This is why in first Distribution team meeting 7 days after your start, we would
like to hear from you about:

* Most interesting information you found and did not know about the company
* Most interesting information you found and did not know about the team
* Piece of information you could not find about the company and the team
* Information that you think could use some more details/improvement

Add yourself to the list of speakers in the [Distribution Team weekly sync](https://docs.google.com/document/d/1rEfZl8l_Z_ndJA6BaBjqccXXFlqVQZ3JkSYclVTy-cI/edit?usp=drive_web&ouid=116856774789128832752) with bullet points from above, and
spend 3-5 minutes max in total sharing your experience with the team.


/label ~"Onboarding"
