<!--

Set issue title as "Scheduling issue for the MAJOR.MINOR release" where MAJOR.MINOR is the GitLab version.

This template is used to create a release's scheduling issue. The scheduling issue
is used to raise visibility to the team on what issues are being looked at as
candidates for scheduling, and provides a place for them to offer up scheduling suggestions.

Engineering manager and Product manager are responsible for scheduling
work that is a direct responsibility of the Distribution team.

Typically the Engineering manager is the one to create this issue as part of their scheduling workflow

Change milestone_title=MAJOR.MINOR

-->

### Product Priorities
<!--

The product manager should provide a prioritized list of major themes for the release

-->
1. ..

### Deliverable Board
Issues on this board have already been reviewed and scheduled for the upcoming release. Each column represents a priority level. The highest ranked issues for each priority level are at the top of each column.

- [Deliverable board](https://gitlab.com/groups/gitlab-org/-/boards/2415614?scope=all&utf8=%E2%9C%93&label_name[]=Deliverable&label_name[]=group%3A%3Adistribution&milestone_title=MAJOR.MINOR)

### For Scheduling board
Used with continuous scheduling process to provide potential candidates, but before assigning a deliverable milestone.
Board includes ~"group::distribution" ~"For Scheduling" issues across gitlab-org projects.

- [For Scheduling board](https://gitlab.com/groups/gitlab-org/-/boards/1282075?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=For%20Scheduling&label_name[]=group%3A%3Adistribution)

### Tech Debt board
Prioritized tech debt issues should be scheduled each release to prevent it from growing unchecked.
Board includes ~"group::distribution" ~"technical debt" issues across gitlab-org projects.

- [Technical Debt board](https://gitlab.com/groups/gitlab-org/-/boards/2361096?&label_name[]=group%3A%3Adistribution&label_name[]=technical%20debt)

### Comments
The team is encouraged to add potential candidates and contextual comments to this issue. These are reviewed weekly with PM, EM and Staff engineers to determine impact and priority.

/cc @gitlab-org/distribution
